<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Expressions.dll</Reference>
</Query>

void Main()
{
	var provider = new ConstructorProvider();
	var container = new Dictionary<string, string>();
	
	var watch = new Stopwatch();
	watch.Start();

	for (var i = 0; i < 500000; i++)
	{
		provider.TEST12<TEST>();
	}
	
	watch.Stop();
	
	watch.ElapsedMilliseconds.Dump();
	
	var t = container.ContainsKey("asd");
}



public class ConstructorProvider
{
	private readonly IDictionary<Type, ParameterInfo[]> container;

	public ConstructorProvider()
	{
		this.container = new Dictionary<Type, ParameterInfo[]>();
	}

	public ParameterInfo[] Get<T>()
	{
		var type = typeof(T);
		if (this.container.ContainsKey(type))
		{
			return this.container[type];
		}
		else
		{
			var value = type.GetConstructors().FirstOrDefault().GetParameters();
			this.container.Add(type, value);
			return value;
		}
	}



	public void TEST123<T>()
	{
		var type = typeof(T);
		var parameters = this.Get<T>();

		var aa = (T)Activator.CreateInstance(type, new object[] { "", "", "", "" });
	}

	public delegate object ObjectActivator(params object[] args);

	public class Cache123
	{
		internal static IDictionary<Type, ObjectActivator> Cache;

		static Cache123()
		{
			Cache = new Dictionary<Type, ObjectActivator>();
		}
	}

	private readonly ParameterExpression paramExp = Expression.Parameter(typeof(object[]), "args");

	public T TEST12<T>()
	{
		NewExpression newExp = null;
		
		if (TypeCache.Cache.ContainsKey(typeof(T))){
			newExp = TypeCache.Cache[typeof(T)];
		}
		else
		{
			newExp = Expression.New(typeof(T).GetConstructors().FirstOrDefault(), new Expression[] { ConstantExpression.Constant("a"), ConstantExpression.Constant(""), ConstantExpression.Constant(""), ConstantExpression.Constant("") });
			TypeCache.Cache.Add(typeof(T), newExp);
		}
		

 //var paramx = Expression.Parameter(typeof(object[]), "args");

		// Create a lambda with the New expression as body and our param object[] as arg
		LambdaExpression lambda = Expression.Lambda(typeof(ObjectActivator), newExp, paramExp);
		
	return (T)((ObjectActivator)lambda.Compile())();

//		// Compile it
//		ObjectActivator compiled = (ObjectActivator)lambda.Compile();
//
//		//TypeCache.Cache.Add(typeof(T), newExp);
//		return (T)compiled(new object[] {"11", "123", "1111", "23232"});

//return default(T);
	}

	public class TypeCache
	{
		internal static IDictionary<Type, NewExpression> Cache;

		static TypeCache()
		{
			Cache = new Dictionary<Type, NewExpression>();
		}
	}
}


public class TEST
{
	public TEST(string a, string b, string c, string d)
	{
		this.A = a;
	}

	public string A { get;}

}

// Define other methods and classes here