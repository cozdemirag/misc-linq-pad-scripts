<Query Kind="Program" />

void Main()
{
	var ctor = typeof(Car).GetConstructors().FirstOrDefault();
	var paramsInfo = ctor.GetParameters();
	
	//var paramDefinitions = new Expression[paramsInfo.Length];
	var delegateCallSignature = Expression.Parameter(typeof(object[]), "args");
	
	
	var ctorSignature = paramsInfo
		.Select(
			(it, index) => 
				Expression.Convert(Expression.ArrayAccess(delegateCallSignature, Expression.Constant(index)), it.ParameterType))
		.ToArray();
	
	var ctorExpression = Expression.New(ctor, ctorSignature);
	
	var compiled = Expression.Lambda<ConstructorDelegate<Car>>(ctorExpression, delegateCallSignature).Compile();


	compiled(1, "a", true);
}

public delegate T ConstructorDelegate<T>(params object[] args);

public class Car
{
	public Car(int a, string b, bool c)
	{
		this.A = a;
		this.B = b;
		this.C = c;
	}

	public int A { get; }
	public string B { get; }
	public bool C { get;}
}

// Define other methods and classes here