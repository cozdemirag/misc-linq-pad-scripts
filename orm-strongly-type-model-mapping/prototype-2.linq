<Query Kind="Program" />

void Main()
{
	var watch = new Stopwatch();
	watch.Start();

	for (var i = 0; i < 1000; i++)
	{
		GetActivator<TEST>(typeof(TEST).GetConstructors().FirstOrDefault())("", "", "", "");
	}

	watch.Stop();

	watch.ElapsedMilliseconds.Dump();
}


public class TEST
{
	public TEST(string a, string b, string c, string d)
	{
		this.A = a;
		this.B = b;
		this.C = c;
		this.D = d;
	}

	public string A { get; }
	public string B { get; }
	public string C { get; }
	public string D { get;}

}

public void ActivatorCreate<T>(params object[]  parameters)
{
	var type = typeof(T);

	var aa = (T)Activator.CreateInstance(type, parameters);
}

public delegate T ObjectActivator<T>(params object[] args);

public static ObjectActivator<T> GetActivator<T>
	(ConstructorInfo ctor)
{
	Type type = ctor.DeclaringType;
	
	if (TypeCache.Cache.ContainsKey(type))
	{
		return (ObjectActivator<T>)TypeCache.Cache[type];
	}
	
	
	ParameterInfo[] paramsInfo = ctor.GetParameters();

	//create a single param of type object[]
	ParameterExpression param =
		Expression.Parameter(typeof(object[]), "args");

	Expression[] argsExp =
		new Expression[paramsInfo.Length];

	//create a typed expression of each ctr parameter
	for (int i = 0; i < paramsInfo.Length; i++)
	{
		Expression index = Expression.Constant(i);
		Type paramType = paramsInfo[i].ParameterType;

		Expression paramAccessorExp =
			Expression.ArrayIndex(param, index);

		Expression paramCastExp =
			Expression.Convert(paramAccessorExp, paramType);

		argsExp[i] = paramCastExp;
	}

	//create ctor with the args 
	NewExpression newExp = Expression.New(ctor, argsExp);

	//creates a ctor del with the typed params
	LambdaExpression lambda =
		Expression.Lambda(typeof(ObjectActivator<T>), newExp, param);

	ObjectActivator<T> compiled = (ObjectActivator<T>)lambda.Compile();
	
	TypeCache.Cache.Add(type, compiled);
	return compiled;
}

public class TypeCache
{
	internal static IDictionary<Type, object> Cache;

	static TypeCache()
	{
		Cache = new Dictionary<Type, object>();
	}
}