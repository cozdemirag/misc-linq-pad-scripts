<Query Kind="Program" />

void Main()
{
	var instance = new TEST();
	(instance.GetType().Name + "_" + instance.GetHashCode()).Dump();

	var watch = new Stopwatch();
	watch.Start();

	for (var i = 0; i < 100000; i++)
	{

		DENEME2<string>(instance);
	}
	instance.GetHashCode().Dump();

	for (var i = 0; i < 100000; i++)
	{

		DENEME2<int>(instance);
	}
	instance.GetHashCode().Dump();

	watch.Stop();
	watch.ElapsedMilliseconds.Dump();
	
}

public void DENEME2<T>(TEST instance)
{
	var methodInfo = instance.GetMI().MakeGenericMethod(typeof(T));

	methodInfo.Invoke(instance, new object[] {"a", "b"});
}

private Dictionary<Type, Func<string, string, int>> container = new Dictionary<Type, Func<string, string, int>>();

public void DENEME1<T>(TEST instance)
{
	var type = typeof(T);
	if (container.ContainsKey(type)){
		container[type]("b", "x");
		return;
	}
	
	var methodInfo = instance.GetMI().MakeGenericMethod(type);
	var paramsInfo = methodInfo.GetParameters();

	var paramDefinitions = new ParameterExpression[paramsInfo.Length];

	for (var i = 0; i < paramsInfo.Length; i++)
	{
		var param = paramsInfo[i];
		paramDefinitions[i] = Expression.Parameter(param.ParameterType, param.Name);
	}

	var callExp = Expression.Call(Expression.Constant(instance), methodInfo, paramDefinitions);

	var exp = Expression.Lambda<Func<string, string, int>>(callExp, paramDefinitions);
	var compiled = exp.Compile();
	container.Add(type, compiled);
	
	compiled("a", "a");

}


public class TEST
{
	public MethodInfo GetMI()
	{
		return this.GetType().GetMethod(nameof(this.CALLME), BindingFlags.Instance | BindingFlags.Public);

	}
	
	
	
	public int CALLME<T>(string a, string b)
	{
		//typeof(T).Dump();
		var ca = Convert.ToChar(a);
		var cb = Convert.ToChar(b);

		return Convert.ToInt32(ca) + Convert.ToInt32(cb);

	}

}

// Define other methods and classes here