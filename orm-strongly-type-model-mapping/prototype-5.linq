<Query Kind="Program" />

void Main()
{
	//var entityFactory = new EntityFactory();
	var entityFactory = new CompiledLambaEntityFactory();

	var values = new Dictionary<string, object>() { { "a", "1" }, { "d", "4" }, { "b", "2" }, { "c", "3" } };
	var values2 = new Dictionary<string, object>() {
	{ "letters", new List<string>() { "a"}},
	
	{"t2", new List<TEST>() {new TEST("", "", "", ""), new TEST("", "", "", ""), new TEST("", "", "", ""), new TEST("", "", "", "")}},
	{"t", new TEST("", "", "", "") },
	};
	var watch = new Stopwatch();
	watch.Start();

	for (var i = 0; i < 10000; i++)
	{
		//ActivatorCreate<TEST>("", "", "", "");
		
		entityFactory.Create<TEST2>(values2);
		//GetActivator<TEST>(typeof(TEST).GetConstructors().FirstOrDefault())(values);
	}

	watch.Stop();

	watch.ElapsedMilliseconds.Dump();
}

public class TEST2
{
	public TEST2(List<string> letters, TEST t, List<TEST> t2){
		this.ltets = letters;
		this.t = t;
		this.t2 = t2;
	}

	public List<string> ltets { get; }
	public TEST t { get; }
	public List<TEST> t2
	{
		get;}
}

public class TEST
{
	public TEST(string a, string b, string c, string d)
	{
		this.A = a;
		this.B = b;
		this.C = c;
		this.D = d;
	}

	public string A { get; }
	public string B { get; }
	public string C { get; }
	public string D { get; }

}

public class EntityFactory
{
	public EntityFactory()
	{
	}

	public T Create<T>(Dictionary<string, object> values)
	{
		var type = typeof(T);
		var constructor = type.GetConstructors().FirstOrDefault();

		if (null == constructor)
		{
			return default(T);
		}

		var ctorParameters = new List<object>();

		foreach (var param in constructor.GetParameters().OrderBy(it => it.Position))
		{
			var ctorParamValue = values.Where(it => it.Key.ToLower() == param.Name.ToLower()).Select(it => it.Value).FirstOrDefault();
			ctorParameters.Add(ctorParamValue as string);
		}

		return (T)Activator.CreateInstance(type, ctorParameters.ToArray());
	}
}
public delegate T ObjectActivator<out T>(params object[] args);

public class CompiledLambaEntityFactory
{
	private static readonly ParameterExpression ParameterExpression = Expression.Parameter(typeof(object[]), "args");

	private readonly IDictionary<Type, TypeActivationInfo> container;

	public CompiledLambaEntityFactory()
	{
		this.container = new Dictionary<Type, TypeActivationInfo>();
	}


	public T Create<T>(Dictionary<string, object> values)
	{
		var type = typeof(T);

		if (this.container.ContainsKey(type))
		{
			var found = this.container[type];
			
			
			return found.Create<T>(values);//((ObjectActivator<T>)f)(values.Select(it => it.Value).ToArray());
		}

		var ctor = type.GetConstructors().FirstOrDefault();

		if (ctor == null)
		{
			throw new ArgumentException("Constructor not found");
		}

		var paramsInfo = ctor.GetParameters();

		var argsExp = new Expression[paramsInfo.Length];

		for (var i = 0; i < paramsInfo.Length; i++)
		{
			var index = Expression.Constant(i);
			var paramType = paramsInfo[i].ParameterType;

			var paramAccessorExp =
				Expression.ArrayIndex(ParameterExpression, index);

			var paramCastExp =
				Expression.Convert(paramAccessorExp, paramType);

			argsExp[i] = paramCastExp;
		}

		var newExp = Expression.New(ctor, argsExp);

		var lambda = Expression.Lambda<ObjectActivator<T>>(newExp, ParameterExpression);
		var compiled = lambda.Compile();

		var typeActivator = new TypeActivationInfo(compiled, paramsInfo);
		this.container.Add(type, typeActivator);
		return typeActivator.Create<T>(values);
	}


}

public class TypeActivationInfo
{
	public TypeActivationInfo(object objectActivator, ParameterInfo[] ctorParamInfo)
	{
		this.ObjectActivator = objectActivator;
		this.ParameterInfo = ctorParamInfo.OrderBy(it => it.Position).ToArray();
	}

	public object ObjectActivator { get; }

	public ParameterInfo[] ParameterInfo { get;}

	public T Create<T>(Dictionary<string, object> values)
	{
		return ((ObjectActivator<T>) this.ObjectActivator)(this.OrderValues(values));
	}

	private object[] OrderValues(Dictionary<string, object> values)
	{
		return this.ParameterInfo.Select(it => values[it.Name]).ToArray();
	}
}

// Define other methods and classes here