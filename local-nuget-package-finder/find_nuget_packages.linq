<Query Kind="Program">
  <Reference Relative="..\..\Source\EposNow.V2\EposNow.Api.V3\EposNow.Api.V3\packages\EposNow.V2.Infrastructure.Data.2016.7.8.4\lib\net452\EposNow.V2.Infrastructure.Data.dll">C:\Users\Cagil\Source\EposNow.V2\EposNow.Api.V3\EposNow.Api.V3\packages\EposNow.V2.Infrastructure.Data.2016.7.8.4\lib\net452\EposNow.V2.Infrastructure.Data.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Data.Linq.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Expressions.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Parallel.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Queryable.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
</Query>

private static string REPOSITORY_DIR = @"C:\Users\Cagil\Source\ReposFromAPI\";
private static string SEARCH_KEYWORD = "Infrastructure.Data";
private static string PACKAGE_INFO_FILE_NAME = "packages.config";

void Main()
{
	var files = SearchDirectoryForFile(REPOSITORY_DIR, PACKAGE_INFO_FILE_NAME);
	Console.WriteLine($"Found ({files.Count}) {PACKAGE_INFO_FILE_NAME} in {REPOSITORY_DIR}");

	var items = new List<PackageItem>();
	Console.WriteLine("Reading files...");
	foreach (var file in files)
	{
		items.AddRange(ReadPackageConfigFile(file));
	}

	Console.WriteLine($"Search for '{SEARCH_KEYWORD}' ...");
	var result = items.Where(it => it.id.Contains(SEARCH_KEYWORD)).ToList();


	Console.WriteLine($"FOUND ({result.Count}) files containing '{SEARCH_KEYWORD}' :");
	var sb = new StringBuilder();
	result.ForEach(it =>
	{
		sb.AppendLine(it.file);
		//sb.AppendLine(it.id);
		//sb.AppendLine(it.version);
	});


	Console.WriteLine(sb.ToString());
}

public List<PackageItem> ReadPackageConfigFile(string filename)
{
	var items = new List<PackageItem>();

	using (var reader = XmlReader.Create(filename))
	{
		reader.ReadToFollowing("packages");

		while (reader.Read())
		{
			reader.MoveToContent();

			var id = reader.GetAttribute("id");

			if (string.IsNullOrWhiteSpace(id))
			{
				continue;
			}

			items.Add(new PackageItem { file = filename, id = id, version = reader.GetAttribute("version") });
		}
	}

	return items;

}

public List<string> SearchDirectoryForFile(string dir, string filename)
{
	var files = new List<string>();

	files.AddRange(FindPackagesFileInDir(dir, filename));

	foreach (var dirname in Directory.GetDirectories(dir))
	{
		files.AddRange(SearchDirectoryForFile(dirname, filename));
	}

	return files;
}

public List<string> FindPackagesFileInDir(string dir, string filename)
{
	return Directory.GetFiles(dir).Where(it => it.EndsWith(filename)).ToList();
}


public class PackageItem
{
	public string file { get; set; }
	public string id { get; set; }
	public string version { get; set; }
}



// Define other methods and classes here