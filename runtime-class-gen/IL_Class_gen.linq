<Query Kind="Program">
  <Reference Relative="..\..\Source\EposNow.V2\EposNow.Api.V3\EposNow.Api.V3\packages\EposNow.V2.Infrastructure.Data.2016.7.8.4\lib\net452\EposNow.V2.Infrastructure.Data.dll">C:\Users\Cagil\Source\EposNow.V2\EposNow.Api.V3\EposNow.Api.V3\packages\EposNow.V2.Infrastructure.Data.2016.7.8.4\lib\net452\EposNow.V2.Infrastructure.Data.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Data.Linq.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Expressions.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Parallel.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.Queryable.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.context.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.Emit.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.Emit.ILGeneration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.Emit.Lightweight.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.Extensions.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Reflection.Primitives.dll</Reference>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Reflection.Emit</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
</Query>

void Main()
{
	//	var p = new Product(1, "Apple", true, DateTime.Now, null);
	//	ModuleBuilder a = new ModuleBuilder()
	//	var typeBUilder = TypeBuilder.GetConstructor(typeof(Product), typeof(Product).GetConstructors().FirstOrDefault(it => it.GetParameters().Length > 0));

	//	var taxRate = Create<ITaxRate>(new object[] { 11, "0VAT" });
	//	var newObjec = Create<IProduct>(
	//		new object[] { 
	//		1, 
	//		"Apple", 
	//		true, 
	//		DateTime.Now, 
	//		null, 
	//		taxRate, 
	//		new List<int> { 1, 2, 3, 4, 5 }.ToArray(),
	//		new List<ITaxRate> {
	//			Create<ITaxRate>(new object[]{1, "1VAT"}),
	//			Create<ITaxRate>(new object[]{2, "2VAT"}),
	//			Create<ITaxRate>(new object[]{3, "3VAT"})
	//		}
	//	});

	//Create<ITest>(new object[] { "", "", "", "" });



	//newObjec.Dump();
	//	newObjec.GetType().GetConstructors().Dump();

	var typeActivator = new TypeActivator();

	for (var i = 0; i < 1000000; i++)
	{
		typeActivator.Create<IProduct>(
			1,
			"Apple",
			true,
			DateTime.Now,
			null,
			new List<int> { 1, 2, 3, 4, 5 }.ToArray());
	}
}

public class TypeActivationInfo
{
	public TypeActivationInfo(Type proxyType, ConstructorInfo ctor, List<ParameterInfo> parameters)
	{
		this.ProxyType = proxyType;
		this.TypeName = proxyType.Name;
		this.Constructor = ctor;
		this.Parameters = parameters;
	}

	public Type ProxyType { get; }
	public string TypeName { get; }
	public List<ParameterInfo> Parameters { get; }
	public ConstructorInfo Constructor { get; }
}

public class TypeActivationContext
{
	private AssemblyBuilder assemblyBuilder;

	public TypeActivationContext(string assemblyName)
	{
		this.assemblyBuilder = Thread.GetDomain().DefineDynamicAssembly(new AssemblyName(assemblyName), AssemblyBuilderAccess.Run);
	}

	public ModuleBuilder CreateModule(string moduleName)
	{
		return this.assemblyBuilder.DefineDynamicModule(moduleName);
	}

	public Assembly Assembly => assemblyBuilder;
}

public class TypeActivator
{
	private static TypeActivationContext ActivationContext = new TypeActivationContext("DataMapper");
	private static ConcurrentDictionary<Type, TypeActivationInfo> container = new ConcurrentDictionary<Type, TypeActivationInfo>();

	private ConstructorInfo BaseObjectConstructor { get; }

	public TypeActivator()
	{
		this.BaseObjectConstructor = Type.GetType("System.Object").GetConstructor(new Type[0]);
	}

	public T Create<T>(params object[] args)
	{
		TypeActivationInfo activationInfo;
		if (container.TryGetValue(typeof(T), out activationInfo))
		{
			return (T)Activator.CreateInstance(activationInfo.ProxyType, args);
		}
		else
		{
			return (T)Activator.CreateInstance(this.GenerateProxyTypeFor<T>(), args);
		}
	}

	public Assembly Assembly => ActivationContext.Assembly;

	private Type GenerateProxyTypeFor<T>(bool refTAsInterface = true)
	{
		var module = ActivationContext.CreateModule("Proxies");
		TypeBuilder typeBuilder = module.DefineType(typeof(T).Name + "_Proxy", TypeAttributes.Public | TypeAttributes.Class);

		if (refTAsInterface)
		{
			typeBuilder.AddInterfaceImplementation(typeof(T));
		}

		var ctorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, typeof(T).GetProperties().Where(it => it.CanRead && !it.CanWrite).Select(it => it.PropertyType).ToArray());//typeof(T).GetProperties().Select(it => it.PropertyType).ToArray());
																																																						 // obtain an ILGenerator to emit the IL
		typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);

		ILGenerator generator = ctorBuilder.GetILGenerator();
		var fields = new Dictionary<string, FieldBuilder>();

		foreach (var prop in typeof(T).GetProperties())
		{
			FieldBuilder fieldBuilder = typeBuilder.DefineField($"{prop.Name}_Field",
																			prop.PropertyType, FieldAttributes.Private);
			fields.Add(prop.Name, fieldBuilder);
			PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(prop.Name, System.Reflection.PropertyAttributes.None,
																		 prop.PropertyType, new Type[] { prop.PropertyType });

			propertyBuilder.SetGetMethod(this.BuildGetter(typeBuilder, fieldBuilder, propertyBuilder));

		}

		generator.Emit(OpCodes.Ldarg_0);
		generator.Emit(OpCodes.Call, this.BaseObjectConstructor);
		var paramIndex = 1;
		foreach (var field in fields)
		{
			generator.Emit(OpCodes.Ldarg_0);
			generator.Emit(OpCodes.Ldarg, paramIndex);
			generator.Emit(OpCodes.Stfld, field.Value);
			paramIndex++;
		}

		generator.Emit(OpCodes.Ret);

		var generatedType = typeBuilder.CreateType();
		var typeActivationInfo = new TypeActivationInfo(generatedType, ctorBuilder, (ctorBuilder as ConstructorInfo).GetParameters().ToList());
		container.TryAdd(typeof(T), typeActivationInfo);

		return generatedType;
	}

	private MethodBuilder BuildGetter(TypeBuilder typeBuilder, FieldInfo fieldBuilder, System.Reflection.Emit.PropertyBuilder propertyBuilder)
	{
		const MethodAttributes attributes =
			MethodAttributes.Public |
			MethodAttributes.HideBySig |
			MethodAttributes.SpecialName |
			MethodAttributes.Virtual |
			MethodAttributes.Final;

		var getterBuilder = typeBuilder.DefineMethod("get_" + propertyBuilder.Name, attributes, propertyBuilder.PropertyType, Type.EmptyTypes);

		var ilgen = getterBuilder.GetILGenerator();
		ilgen.Emit(OpCodes.Ldarg_0);
		ilgen.Emit(OpCodes.Ldfld, fieldBuilder);
		ilgen.Emit(OpCodes.Ret);
		return getterBuilder;
	}
}

public T Create<T>()
{
	var assembly = Do<T>();
	var t = (T)assembly.CreateInstance("ITaxRate_Proxy");
	return t;
}

public T Create<T>(object[] args)//int id, string name, bool sell, DateTime createdDate, int? taxrateId)
{
	var assembly = Do<T>();
	AppDomain.CurrentDomain.Load(assembly.FullName);
	var type = assembly.GetType($"{typeof(T).Name}_Proxy");
	//var type=t.GetType();
	//).Dump();
	var ctor = type.GetConstructors().FirstOrDefault(it => it.GetParameters().Length > 0);
	var creator = GetActivatorEmit(ctor);
	//var args = new object[] { id, name, sell, createdDate, taxrateId};

	//var result = creator(args);
	//return (T)result;
	return (T)Activator.CreateInstance(type, args);
}

public Assembly Do<T>()
{
	var ctor = typeof(T).GetConstructors().FirstOrDefault(it => it.GetParameters().Length > 0);
	AssemblyName assemblyName = new AssemblyName();
	assemblyName.Name = "FactorialAssembly";

	// create assembly with one module
	AssemblyBuilder newAssembly = Thread.GetDomain().DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
	ModuleBuilder newModule = newAssembly.DefineDynamicModule("MFactorial");

	// define a public class named "CFactorial" in the assembly
	TypeBuilder myType = newModule.DefineType(typeof(T).Name + "_Proxy", TypeAttributes.Public | TypeAttributes.Class);

	myType.AddInterfaceImplementation(typeof(T));

	// define myfactorial method by passing an array that defines
	// the types of the parameters, the type of the return type,
	// the name of the method, and the method attributes.

	Type[] paramTypes = new Type[0];
	Type returnType = typeof(Int32);
	Type objType = Type.GetType("System.Object");
	ConstructorInfo objCtor = objType.GetConstructor(new Type[0]);

	//MethodBuilder simpleMethod = myType.DefineMethod("myfactorial", MethodAttributes.Public | MethodAttributes.Virtual, returnType, paramTypes);
	var ctorBuilder = myType.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, typeof(T).GetProperties().Where(it => it.CanRead && !it.CanWrite).Select(it => it.PropertyType).ToArray());//typeof(T).GetProperties().Select(it => it.PropertyType).ToArray());
																																																				// obtain an ILGenerator to emit the IL
	myType.DefineDefaultConstructor(MethodAttributes.Public);

	ILGenerator generator = ctorBuilder.GetILGenerator();


	//generator.Emit(OpCodes.

	//	// Ldc_I4 pushes a supplied value of type int32
	//	// onto the evaluation stack as an int32.
	//	// push 1 onto the evaluation stack.
	//	// foreach i less than theValue,
	//	// push i onto the stack as a constant
	//	// multiply the two values at the top of the stack.
	//	// The result multiplication is pushed onto the evaluation
	//	// stack.
	//	generator.Emit(OpCodes.Ldc_I4, 1);
	//
	//	for (Int32 i = 1; i <= theValue; ++i)
	//	{
	//		generator.Emit(OpCodes.Ldc_I4, i);
	//		generator.Emit(OpCodes.Mul);
	//	}
	//
	//	// emit the return value on the top of the evaluation stack.
	//	// Ret returns from method, possibly returning a value.
	//
	var fields = new Dictionary<string, FieldBuilder>();

	foreach (var prop in typeof(T).GetProperties())
	{
		//		FieldBuilder fieldBuilder = myType.DefineField(property.Name.Substring(0, 1).ToLower() + property.Name.Substring(1), property.PropertyType, FieldAttributes.Private | FieldAttributes.HasDefault);
		//		PropertyBuilder propertyBuilder = myType.DefineProperty(property.Name, System.Reflection.PropertyAttributes.HasDefault, property.PropertyType, new[] { property.PropertyType});
		//		MethodBuilder methodBuilder = myType.DefineMethod("get_" + property.Name, MethodAttributes.Public);
		//		var methodGen = methodBuilder.GetILGenerator();
		//		methodGen.Emit(OpCodes.Ldarg_0);
		//		methodGen.Emit(OpCodes.Ldfld, fieldBuilder);
		//		methodGen.Emit(OpCodes.Ret);
		//		propertyBuilder.SetGetMethod(methodBuilder);
		FieldBuilder fieldBuilder = myType.DefineField($"{prop.Name}_Field",
																		prop.PropertyType, FieldAttributes.Private);
		fields.Add(prop.Name, fieldBuilder);
		PropertyBuilder propertyBuilder = myType.DefineProperty(prop.Name, System.Reflection.PropertyAttributes.None,
																	 prop.PropertyType, new Type[] { prop.PropertyType });
		//		MethodBuilder propertyGetter = myType.DefineMethod("get_" + prop.Name, MethodAttributes.Public | MethodAttributes.HideBySig, prop.PropertyType, Type.EmptyTypes);
		//		var ilGenerator = propertyGetter.GetILGenerator();
		//		ilGenerator.Emit(OpCodes.Ldarg_0);
		//		ilGenerator.Emit(OpCodes.Ldfld, fieldBuilder);
		//		ilGenerator.Emit(OpCodes.Ret);
		//		propertyBuilder.SetGetMethod(propertyGetter);

		propertyBuilder.SetGetMethod(BuildGetter(myType, fieldBuilder, propertyBuilder));

	}

	generator.Emit(OpCodes.Ldarg_0);
	generator.Emit(OpCodes.Call, objCtor);
	//	generator.Emit(OpCodes.Nop);
	//	generator.Emit(OpCodes.Nop);
	var paramIndex = 1;
	foreach (var field in fields)
	{
		generator.Emit(OpCodes.Ldarg_0);
		generator.Emit(OpCodes.Ldarg, paramIndex);
		generator.Emit(OpCodes.Stfld, field.Value);
		paramIndex++;
	}

	generator.Emit(OpCodes.Ret);
	//
	//	// encapsulate information about the method and
	//	// provide access to the method metadata
	//	MethodInfo factorialInfo = typeof(IProduct).GetMethod("myfactorial");
	//
	//	// specify the method implementation.
	//	// pass in the MethodBuilder that was returned
	//	// by calling DefineMethod and the methodInfo just created
	//	myType.DefineMethodOverride(simpleMethod, factorialInfo);

	// create the type and return new on-the-fly assembly


	var t = myType.CreateType();
	return newAssembly;

}
public delegate object ObjectActivator(params object[] args);
public static ObjectActivator GetActivatorEmit(ConstructorInfo ctor)
{
	ParameterInfo[] paramsInfo = ctor.GetParameters();
	DynamicMethod method = new DynamicMethod("CreateInstance", typeof(object), new Type[] { typeof(object[]) });
	ILGenerator gen = method.GetILGenerator();
	for (int i = 0; i < paramsInfo.Length; i++)
	{
		Type t = paramsInfo[i].ParameterType;
		gen.Emit(OpCodes.Ldarg_0); // Push array (method argument)
		gen.Emit(OpCodes.Ldc_I4, i); // Push i
		gen.Emit(OpCodes.Ldelem_Ref); // Pop array and i and push array[i]
		if (t.IsValueType)
		{
			gen.Emit(OpCodes.Unbox_Any, t); // Cast to Type t
		}
		else
		{
			gen.Emit(OpCodes.Castclass, t); //Cast to Type t
		}
	}
	gen.Emit(OpCodes.Newobj, ctor);
	gen.Emit(OpCodes.Ret);
	return (ObjectActivator)method.CreateDelegate(typeof(ObjectActivator));
}

private static MethodBuilder BuildGetter(TypeBuilder typeBuilder, FieldInfo fieldBuilder, System.Reflection.Emit.PropertyBuilder propertyBuilder)
{
	const MethodAttributes attributes =
		MethodAttributes.Public |
		MethodAttributes.HideBySig |
		MethodAttributes.SpecialName |
		MethodAttributes.Virtual |
		MethodAttributes.Final;

	var getterBuilder = typeBuilder.DefineMethod("get_" + propertyBuilder.Name, attributes, propertyBuilder.PropertyType, Type.EmptyTypes);

	// Code generation
	var ilgen = getterBuilder.GetILGenerator();
	//	if (propertyBuilder.Name.Contains("ProductID"))
	//	{
	//		ilgen.Emit(OpCodes.Ldc_I4, 1);
	//
	//	}
	//	else
	//	{
	ilgen.Emit(OpCodes.Ldarg_0);
	ilgen.Emit(OpCodes.Ldfld, fieldBuilder);

	//}		 // returning the firstname field
	ilgen.Emit(OpCodes.Ret);
	return getterBuilder;
}

public interface IProduct
{
	int ProductID { get; }
	string Name { get; }
	bool SellOnTill { get; }
	DateTime CreatedDate { get; }
	int? TaxRateID { get; }
	//ITaxRate TaxRate { get; }
	IEnumerable<int> Ids { get; }
	//IEnumerable<ITaxRate> TaxRates { get; }
}

public interface ITaxRate
{
	int TaxRateID { get; }
	string Name { get; }
}

public interface ITest
{
	string A { get; }
	string B { get; }
	string C { get; }
	string D { get; }

}

// Define other methods and classes here
//public class Product : IProduct
//{
//	public Product(int productId, string name, bool sellOnTill, DateTime createdDate, int? taxRateId)
//	{
//		this.ProductID = productId;
//		this.Name = name;
//		this.SellOnTill = sellOnTill;
//		this.CreatedDate = createdDate;
//		this.TaxRateID = taxRateId;
//	}
//
//	public int ProductID { get; }
//	public string Name { get; }
//	public bool SellOnTill { get; }
//	public DateTime CreatedDate { get; }
//	public int? TaxRateID { get;}
//}